# Australian Suburbs plugin for Craft CMS 4.x

Australian suburbs data

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 4.0.0 or later.

## Installation

To install the plugin, follow these instructions.

1. Add this repo to your composer.json

```
{
... 
"repositories": [
   { 
      "type": "git", 
      "url":"https://bitbucket.org/SimpleIM/australian-suburbs"
   }
]
```
]

2. Then tell Composer to load the plugin:

        composer require simple/australian-suburbs

4. In the Control Panel, go to Settings → Plugins and click the “Install” button for Australian Suburbs.

## Australian Suburbs Overview

-Insert text here-

## Configuring Australian Suburbs

-Insert text here-

## Using Australian Suburbs

-Insert text here-

## Australian Suburbs Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Simple Integrated Marketing](https://simple.com.au)
