<?php

namespace simple\australiansuburbs\models;

use craft\base\Model;
use Craft;
use simple\australiansuburbs\records\Suburb;

class SuburbsCriteria extends Model
{
    public $q;
    public $isFuzzySearch = false;

    public $state;

    public $lat;
    public $lng;
    public $radiusKm = 100;

    public function rules()
    {
        $rules = [
            [
                'lat', 'required', 'when' => function($model) {
                return $model->lng !== null;
            }
            ],
            [
                'lng', 'required', 'when' => function($model) {
                return $model->lat !== null;
            }
            ],
            ['state', 'in', 'range' => ['ACT', 'NSW', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA']]
        ];
        return array_merge(parent::rules(), $rules);
    }

    public function all()
    {
        $this->validate();
        if ($this->hasErrors()) {
            return Craft::dd($this->errors);
        }
        return $this->_getQuery()->all();
    }

    public function one() {
        $this->validate();
        if ($this->hasErrors()) {
            return Craft::dd($this->errors);
        }
        return $this->_getQuery()->one();
    }

    public function _getQuery() {
        $base = Suburb::find();
        if ($this->q) {
            if (strlen($this->q) == 4 && is_numeric($this->q)) {
                if ($this->isFuzzySearch) {
                    $base = $base->where(['like','postcode',$this->q]);
                } else {
                    $base = $base->where(['postcode' => $this->q]);
                }
            } else {
                if ($this->isFuzzySearch) {
                    $base = $base->where(['like','locality',strtoupper($this->q)]);
                } else {
                    $base = $base->where(['locality' => strtoupper($this->q)]);
                }
            }
        }
        if ($this->state) {
            $base = $base->andWhere(['state' => $this->state]);
        }
        return $base;
    }

}