<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs\services;

use craft\helpers\Json;
use GuzzleHttp\Client;
use simple\australiansuburbs\AustralianSuburbs;

use Craft;
use craft\base\Component;
use simple\australiansuburbs\records\Suburb;

/**
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 */
class Suburbs extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function updateSuburbs()
    {
        // Delete all existing suburbs data
        $allSuburbs = Suburb::find()->all();
        foreach ($allSuburbs as $suburb) {
            $suburb->delete();
        }

        $url = "https://www.matthewproctor.com/Content/postcodes/australian_postcodes.json";

        $client = new Client();
        $response = $client->get("https://www.matthewproctor.com/Content/postcodes/australian_postcodes.json");
        $data = Json::decodeIfJson($response->getBody()->getContents());

        $count = 0;
        $newSuburbs = [];
        foreach ($data as $row) {
            $postcode = $row["postcode"] ?? "";
            $locality = $row["locality"] ?? "";
            $state = $row["state"] ?? "";
            $lat = $row["lat"] ?? "";
            $long = $row["long"] ?? "";

            if ($postcode && $locality && $lat && $long) {
                $suburb = new Suburb();
                $suburb->postcode = $postcode;
                $suburb->locality = $locality;
                $suburb->state = $state;
                $suburb->lat = $lat;
                $suburb->long = $long;
                $suburb->save();
                $newSuburbs[] = $suburb;
                $count++;
            }
        }

        return $newSuburbs;
    }

}
