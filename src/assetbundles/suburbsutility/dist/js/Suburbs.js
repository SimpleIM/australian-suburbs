/**
 * Australian Suburbs plugin for Craft CMS
 *
 * Suburbs Utility JS
 *
 * @author    Simple Integrated Marketing
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 * @link      https://simple.com.au
 * @package   AustralianSuburbs
 * @since     1.0.0
 */
