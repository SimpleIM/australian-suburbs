<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs\assetbundles\suburbsutility;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 */
class SuburbsUtilityAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@simple/australiansuburbs/assetbundles/suburbsutility/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/Suburbs.js',
        ];

        $this->css = [
            'css/Suburbs.css',
        ];

        parent::init();
    }
}
