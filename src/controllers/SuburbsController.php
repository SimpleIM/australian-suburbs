<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs\controllers;

use simple\australiansuburbs\AustralianSuburbs;

use Craft;
use craft\web\Controller;
use simple\australiansuburbs\models\SuburbsCriteria;

/**
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 */
class SuburbsController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'search'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return;
    }

    /**
     * @return mixed
     */
    public function actionImport()
    {
        $newSuburbs = AustralianSuburbs::$plugin->suburbs->updateSuburbs();
        Craft::$app->session->setNotice(count($newSuburbs)." records have been imported");
        return;
    }

    public function actionSearch() {
        $q = Craft::$app->request->getRequiredParam("q");
        $state = Craft::$app->request->getParam("state", "");
        $state = strtoupper($state);
        // q needs to at least 3 characters long
        if (strlen($q) < 3) {
            return $this->asJson([]);
        }
        $suburbs = new SuburbsCriteria([
            'q' => $q,
            'isFuzzySearch' => true,
            'state' => $state,
        ]);
        return $this->asJson($suburbs->all());
    }
}
