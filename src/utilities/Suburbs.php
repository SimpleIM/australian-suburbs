<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs\utilities;

use simple\australiansuburbs\AustralianSuburbs;
use simple\australiansuburbs\assetbundles\suburbsutility\SuburbsUtilityAsset;

use Craft;
use craft\base\Utility;

/**
 * Australian Suburbs Utility
 *
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 */
class Suburbs extends Utility
{
    // Static
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('australian-suburbs', 'Suburbs');
    }

    /**
     * @inheritdoc
     */
    public static function id(): string
    {
        return 'australian-suburbs';
    }

    /**
     * @inheritdoc
     */
    public static function iconPath(): ?string
    {
        return Craft::getAlias("@simple/australiansuburbs/assetbundles/suburbsutility/dist/img/icon.svg");
    }

    /**
     * @inheritdoc
     */
    public static function badgeCount(): int
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public static function contentHtml(): string
    {
        Craft::$app->getView()->registerAssetBundle(SuburbsUtilityAsset::class);

        return Craft::$app->getView()->renderTemplate(
            'australian-suburbs/_components/utilities/suburbs_content',
            [
            ]
        );
    }
}
