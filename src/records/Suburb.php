<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs\records;

use craft\db\ActiveQuery;
use simple\australiansuburbs\AustralianSuburbs;

use Craft;
use craft\db\ActiveRecord;

/**
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 * @property string $postcode
 * @property string $locality
 * @property string $state
 * @property float $lat
 * @property float $long
 */
class Suburb extends ActiveRecord
{
    // Public Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%australiansuburbs_suburbs}}';
    }

}
