<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs;

use craft\helpers\UrlHelper;
use simple\australiansuburbs\services\Suburbs as SuburbsService;
use simple\australiansuburbs\variables\AustralianSuburbsVariable;
use simple\australiansuburbs\twigextensions\AustralianSuburbsTwigExtension;
use simple\australiansuburbs\utilities\Suburbs as SuburbsUtility;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\services\Utilities;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;

/**
 * Class AustralianSuburbs
 *
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 *
 * @property  SuburbsService $suburbs
 */
class AustralianSuburbs extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var AustralianSuburbs
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public string $schemaVersion = '1.0.0';

    /**
     * @var bool
     */
    public bool $hasCpSettings = false;

    /**
     * @var bool
     */
    public bool $hasCpSection = false;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Craft::$app->view->registerTwigExtension(new AustralianSuburbsTwigExtension());

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'australian-suburbs/suburbs';
            }
        );

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'australian-suburbs/suburbs/do-something';
            }
        );

        Event::on(
            Utilities::class,
            Utilities::EVENT_REGISTER_UTILITY_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = SuburbsUtility::class;
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('australianSuburbs', AustralianSuburbsVariable::class);
            }
        );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    Craft::$app->response->redirect(
                        "/admin/utilities/australian-suburbs"
                    )->send();
                }
            }
        );

        Craft::info(
            Craft::t(
                'australian-suburbs',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
