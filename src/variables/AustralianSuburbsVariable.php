<?php
/**
 * Australian Suburbs plugin for Craft CMS 3.x
 *
 * Australian suburbs data
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2022 Simple Integrated Marketing
 */

namespace simple\australiansuburbs\variables;

use simple\australiansuburbs\AustralianSuburbs;

use Craft;
use simple\australiansuburbs\models\SuburbsCriteria;
use simple\australiansuburbs\records\Suburb;

/**
 * @author    Simple Integrated Marketing
 * @package   AustralianSuburbs
 * @since     1.0.0
 */
class AustralianSuburbsVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @return Suburb[]
     */
    public function all()
    {
        return Suburb::find()->all();
    }

    /**
     * @param $params
     * @return SuburbsCriteria
     * @throws \yii\base\InvalidConfigException
     */
    public function query($params = [])
    {
        $criteria = new SuburbsCriteria();
        return Craft::configure($criteria, $params);
    }
}
